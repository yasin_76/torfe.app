@extends('layout.master')
@section('fluid-container')
    <!-- <div class="row">
    <div class="col-xs-12">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
        @endif
    @endforeach
    @if ($errors->all())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul class="list-unstyled">
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
                </ul>
            </div>
@endif
            </div>
        </div> -->

    <!-- <div style="position: absolute;z-index: 1;max-width: 80%">
        <div id="large-header">
            <canvas id="demo-canvas" style="max-width: 100%;"></canvas>
        </div>
    </div> -->
    <!-- background: linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%); -->

    <div class="row text-center" style="background-image: url('/public/img/a33.jpg');
background-position: cover; background-attachment: fixed;">
<div class="seperate"></div>
        <div class="col-xs-10 col-xs-offset-1 ">

            <h1 class="bold  hug-size" style="color: #fff">
                مرکز نوآوری و کارآفرینی طرفه نگار
            </h1>
            <h3 style="color: #fff" >
                <!-- ما آماده ایم تا شما را هرچه سریع‌تر به اهدافتان برسانیم -->
                فراسوی نیازهای شما
            </h3>
        </div>
        <div class="col-xs-offset-0 col-xs-11">
            <div class="">
                @if(!$mobile)
                    <div class="ip-slideshow-wrapper">
                        <nav>
                            <span class="ip-nav-left"></span>
                            <span class="ip-nav-right"></span>
                        </nav>
                        <div class="ip-slideshow"></div>
                    </div>
                @endif
            </div><!-- /container -->
        </div>
    </div>

    <div class="row" style="background-color: white">

    	<div class="seperate"></div>
        <div class="seperate"></div>
        <div class="row">
            <div class="col-xs-12 text-center">
                <a href="register-idea" class="button button--moema">
                    <span class="glyphicon glyphicon-pencil "></span>
                    ثبت درخواست
                </a>
            </div>
        </div>
        
        
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        
        @endsection
        @push('script')
            <script type="text/javascript" src="/public/js/jquery.imgslider.js"></script>
            <script type="text/javascript">
                $(function () {

                    $('#fs-slider').imgslider();

                });
            </script>
            <script src="/public/js/classie2.js"></script>
            <!-- <script src="/public/js/stepsForm.js"></script> -->
            <!-- <script>
                var theForm = document.getElementById( 'theForm' );

                new stepsForm( theForm, {
                    onSubmit : function( form ) {
                        // hide form
                        classie.addClass( theForm.querySelector( '.simform-inner' ), 'hide' );

                        /*
                        form.submit()
                        or
                        AJAX request (maybe show loading indicator while we don't have an answer..)
                        */

                        // let's just simulate something...
                        var messageEl = theForm.querySelector( '.final-message' );
                        messageEl.innerHTML = 'ممنون از پر کردن فرم ما ،به زودی می‌بینیمت';
                        classie.addClass( messageEl, 'show' );
                    }
                } );
            </script> -->
            <script src="/public/js/particlesSlideshow.js"></script>

            <script>
                scrollDown = function () {
                    $('html,body').animate({
                        scrollTop: $("#restaurant-list").offset().top
                    }, 'slow');
                }
                move = $(window).scrollTop() / 12;
                $('#scroled2').animate({
                    left: 10 + move + 'px',
                    opacity: '1',
                }, 800);
                $('#scroled').animate({
                    right: 10 + move + 'px',
                    opacity: '1',
                }, 800);
                $(window).scroll(function () {
                    if ($(window).scrollTop() < 1000) {
                        move = $(window).scrollTop() / 4;
                        $('#scroled2').css("left", 10 + move / 2 + "px");
                        $('#scroled').css("right", 10 + move / 2 + "px");
                    }
                });
            </script>
            <!-- <script src="/public/js/demo-2.js"></script> -->
    @endpush

