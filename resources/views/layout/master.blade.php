<!DOCTYPE html>
<html lang="{{ Lang::locale() }}" dir="{{ Lang::locale() == 'fa' ? 'rtl' : 'ltr' }}">
	<head>
		@include('common.header')
	</head>
	<body>
		@include('common.navbar')
		<div id="body_id">
			<div class="seperate"></div>
			<div class="half-seperate"></div>
			<div class="half-seperate"></div>
			<div class="container-fluid background-container-fluid">
				@yield('fluid-container')
			</div>
			<div class="container">
				<div class="background-container">
					@yield('container')
				</div>
			</div>
		</div>
		@include('common.footer')
		@stack('script')
		@include('common.script')
	</body>
</html>