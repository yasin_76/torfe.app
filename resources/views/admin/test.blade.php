<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Interactive Particles Slideshow with HTML5 Canvas</title>
		<meta name="description" content="Interactive Particles Slideshow Experiment with HTML5 Canvas" />
		<meta name="keywords" content="html5, canvas, slideshow, interactive, particles, web development" />
		<meta name="author" content="Francesco Trillini for Codrops" />
		<link rel="shortcut icon" href="../favicon.ico"> 
		
	</head>
	<body>
		
	</body>
</html>