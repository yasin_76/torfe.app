@extends('admin.dashboard')
@section('content')
<div class="row">
	<div class="col-xs-12">
		<form enctype="multipart/form-data" method="post" action="/admin/type">
		{{ csrf_field() }}
		<div class="panel panel-danger">
		<div class="panel-heading">
			ویرایش دسته بندی ها:
		</div>
		<div class="panel-body">
			@if ($errors->all())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        	@endif
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
            @endif
            @endforeach
            <div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<td width="150">نام دسته بندی</td>
					<td><input type="hidden" name="id" value="{{  $type_edited->id  or old('id')  }}">
					<input type="text" name="name" class="form-control"	value="{{  $type_edited->name  or old('name')  }}" required></td>
				</tr>
				<tr>
					<td>مادر</td>
					<td>
					<select class="form-control" name="type_id">
						<option value="">بی مادر</option>
						@foreach(\App\Models\Type::get() as $type)
						<option value="{{ $type->id }}"
						{{ ( isset($type_edited) ? $type_edited->type_id : 1 ) == $type->id ? "selected" : ""}} > 
							{{ $type->type ? $type->type->name.' >' : ''  }}  {{ $type->name }}
						</option>
						@endforeach
					</select>
					</td>
				</tr>
			</table>
			</div>
			<button type="submit" class="btn btn-success btn-block">ذخیره اطلاعات دسته بندی</button>
        </div>
		</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
		<div class="panel-heading">
			دسته بندی ها:
		</div>
		<div class="panel-body">
		<div class="reponsive-table">
			<table class="table table-striped">
			<thead>
			<tr>
				<th>
				نام 
				</th>
				<th>
				مادر
				</th>
				<th>
				ویرایش
				</th>
				<th>
				 حذف
				</th>
			</tr>
			</thead>
			<tbody>
			@foreach($types as $type)
			<tr>
				<td>
					<h4>
						{{ $type->name }}
					</h4>
				</td>
				<td>
					@if($type->type)
						@if($type->type->type)						
						{{ $type->type->type->name }}
						> 
						@else
						@endif					
					{{ $type->type->name }}
					@else
					-
					@endif
				</td>
				<td>
					<a class="btn btn-info btn-sm" href="/admin/type/edit/{{ $type->id }}">
					ویرایش
					</a>
				</td>
				<td>
					<a class="btn btn-danger btn-sm" href="/admin/type/remove/{{ $type->id }}">
					حذف
					</a>					
				</td>
			</tr>
			@endforeach
			</tbody>
			</table>
			</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="text-center">
			{{ $types->links() }}
		</div>
	</div>
</div>
@endsection
@push('script')
<script>
 	
</script>
@endpush