@extends('admin.dashboard')
@section('content')
<div class="row">
	<div class="col-xs-12">
	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
        @endif
    @endforeach
	@if ($errors->all())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul class="list-unstyled">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-danger">
		<div class="panel-heading">ارسال ایمیل </div>
		<div class="panel-body">
			<form action="/admin/notice/email" method="post">
					{{ csrf_field() }}
				<p class="bold">متن پیام:</p>
				<textarea class="form-control" name="email"></textarea>
				<div class="seperate"></div>
				<p class="bold">گیرندگان ایمیل:</p>
			  	<div class="radio-style">
				    <div class="radio-button">
				    	<input type="radio" value="all" id="index" name="who-email" class="radio-button">
				    	<label for="index"><span class="radio">تمام کاربران</span> </label>
				    </div>
				    <div class="radio-button">
				    	<input type="radio" value="address" id="index2" name="who-email" class="radio-button">
				    	<label for="index2"><span class="radio">کاربرانی که سفارش تابه حال داده اند</span> </label>
				    </div>
				    <div class="radio-button">
				    	<input type="radio" value="address" id="index3" name="who-email" class="radio-button">
				    	<label for="index3"><span class="radio">مدیران رستوران ها</span> </label>
				    </div>
				</div>
				<div class="seperate"></div>
				<div class="row">
					<div class="col-xs-4">
						<button type="submit" class="btn btn-danger">ارسال</button>
					</div>
				</div>
			</form>
		</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-warning">
			<div class="panel-heading">ارسال اس ام اس</div>
			<div class="panel-body">
				<form action="/admin/notice/sms" method="post">
					{{ csrf_field() }}
					<p class="bold">متن پیام:</p>
					<textarea class="form-control" name="sms"></textarea>
					<div class="seperate"></div>
					<p class="bold">گیرندگان اس ام اس:</p>
				  	<div class="radio-style">
					    <div class="radio-button">
					    	<input type="radio" value="all" id="all" name="who" class="radio-button">
					    	<label for="all"><span class="radio">تمام کاربران</span> </label>
					    </div>
					    <div class="radio-button">
					    	<input type="radio" value="user_with_order" id="user_with_order" name="who" class="radio-button">
					    	<label for="user_with_order"><span class="radio">کاربرانی که سفارش تابه حال داده اند</span> </label>
					    </div>
					    <div class="radio-button">
					    	<input type="radio" value="restaurant_manager" id="restaurant_manager" name="who" class="radio-button">
					    	<label for="restaurant_manager"><span class="radio">مدیران رستوران ها</span> </label>
					    </div>
					</div>
					<div class="seperate"></div>
					<div class="row">
						<div class="col-xs-4">
							<button type="submit" class="btn btn-danger">ارسال</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

