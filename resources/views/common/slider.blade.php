
<div id="myCarousel" class="carousel slide no-margin" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      	<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
      	<li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      	<div class="item active">
	        <img src="/public/img/00slider1.jpg" alt="slider1" style="width: 100%">
	        <div class="carousel-caption">
                <div style="background: rgba(0, 0, 0, 0.2);">
	           <h3 style="color: white">راه موفقیت، همیشه در حال ساخت است؛ موفقیت پیش رفتن است، نه به نقطه پایان رسیدن.
             </h3>
                </div>
             <h5>
             "آنتونی رابینز"
            </h5>
	           <p></p>
	        </div>
      	</div>

      	<div class="item">
	        <img src="/public/img/00slider2.jpg" alt="slider2" style="width: 100%">
	        <div class="carousel-caption">
	           <h3>طرفه نگار
بزرگ ترین تولید کننده نرم افزارهای مالی در ایران با بیش از نیم میلیون کاربر</h3>
	           <p></p>
	        </div>
      	</div>

        <div class="item">
            <img src="/public/img/00slider3.jpg" alt="slider3" style="width: 100%">
            <div class="carousel-caption">
                <div style="background-color: rgba(0, 0, 0, 0.4);">
                <h3 style="color: white">
                  در دنیای مدرن کسب و کار، این بی فایده است که متفکری خلاق و مبتکر باشی مگر اینکه بتوانی چیزی که خلق می‌کنی را بفروشی.
                  <br>
" دیوید اگیلوی " 

                </h3>
                </div>
            </div>
        </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      	<span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      	<span class="sr-only">Next</span>
    </a>
</div>	

