@extends('layout.master')
@section('container')
<div class="seperate"></div>
<div class="row">
	<div class="col-xs-12">
		<div class="seperate"></div>
	 	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul class="list-unstyled">
                        <li>{{ Session::get('alert-' . $msg) }}</li>
                    </ul>
                </div>
            @endif
        @endforeach
        @if ($errors->all())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        @endif
	</div>	
	@if(!Auth::id())
	<div class="col-sm-8">
		<div class="panel panel-info">
			<div class="panel-heading">
				ورود مهمان:<small> ( تنها کافی است شماره همراه خود را وارد کنید. )</small>
			</div>
	        <div class="panel-body">
		        <form method="post" action='/user/register'>
		            {{ csrf_field() }}
		            <div class="form-group col-xs-6">
		                <label for="first_name">نام:<small style="color:#777"> (اختیاری)</small></label>
		                <input type="text" class="form-control" id="first_name" name="first_name" value="{{ old('first_name','کاربر') }}" required>
		            </div>
		            <div class="form-group col-xs-6">
		                <label for="last_name">نام خانوادگی:<small style="color:#777"> (اختیاری)</small></label>
		                <input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name','مهمان')}}" required>
		            </div>
		            <div class="form-group col-xs-6">
		                <label for="phone">شماره تماس:<small style="color:#777"> (مثال: 09129876543)</small></label>
		                <input type="text" class="form-control ltr" id="phone" name="phone" value="{{ old('phone') }}" required>
		            </div>
		            <div class="form-group col-xs-6">
		                <label for="password">رمز عبور:</label>
		                <input type="text" class="form-control ltr" id="password" name="password" required>
		            </div>
	            	<button type="submit" class="btn btn-primary btn-block">ثبت نام</button>
		        </form>
	        </div>  
        </div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-success">
			<div class="panel-heading">
				ورود کاربران 
			</div>
	    	<div class="panel-body">
			    <form method="post" action='/user/login'>
			        {{ csrf_field() }}
			        <div class="form-group">
			            <label for="phone">شماره همراه:</label>
			            <input type="text" class="form-control ltr" id="phone" name="phone" required>
			        </div>
			        <div class="form-group">
			            <label for="pwd">رمز عبور: 
			            	<a href="javascript:void(0)" data-toggle="modal" 
			            		data-target="#sms-modal">
			                	رمز عبور خود را فراموش کرده اید؟
			                </a> 
		                </label>
			            <input type="text" class="form-control ltr" id="pwd" name="password" required>
			        </div>
			        <button type="submit" class="btn btn-success btn-block">ورود</button>
			    </form>
	    	</div>
	    </div>
    </div>
	@else
	<div class="col-xs-12">
		<choose-address segment2="{{ Request::segment(2) }}" :user-id="{{ \Auth::user() ? \Auth::id() : 0 }}"></choose-address>
    </div>
  	<button type="button" class="btn btn-info btn-block" data-toggle="collapse" data-target="#demo">ثبت آدرس جدید</button>
    <div class="seperate"></div>
  	<div id="demo" class="collapse ">  	
	<form method="post" action="/put-address">
		{{ csrf_field() }}
		<div class="form-horizontal">
			<div class="form-group">
			    <label for="name" class="col-sm-3 control-label">نام و نام‌خانوادگی تحویل گیرنده</label>
			    <div class="col-sm-3">
			      	<input type="text" class="form-control" id="name" name="name" value="{{Auth::user()->first_name}} {{Auth::user()->last_name}}" required>
			    </div>
			    <label for="phone" class="col-sm-2 control-label">شماره تماس ضروری (همراه)</label>
			    <div class="col-sm-3">
			      	<input type="text" class="form-control" id="phone" name="phone" value="{{Auth::user()->phone}} " required>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="sabet" class="col-sm-3 control-label">شماره تلفن ثابت تحویل گیرنده (اختیاری)</label>
			    <div class="col-sm-3">
			      	<input type="text" class="form-control" id="sabet" name="sabet">
			    </div>
			    <label for="postcode" class="col-sm-2 control-label">کد پستی (اختیاری)</label>
			    <div class="col-sm-3">
			      	<input type="text" class="form-control" id="postcode" name="postcode">
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="province" class="col-sm-3 control-label">استان</label>
			    <div class="col-sm-2">
					<select id="province" class="form-control" name="province">
						@foreach(\App\Models\Province::get() as $province)
						<option value="{{ $province->id }}">
							{{ $province->name }}
						</option>
						@endforeach
					</select>		    
				</div>
			    <label for="city" class="col-sm-1 control-label">شهر</label>
			    <div class="col-sm-2">
			      	<select id="city" class="form-control" name="city">
					</select>	
			    </div>
			    <label for="state" class="col-sm-1 control-label">منطقه</label>
			    <div class="col-sm-2">
			      	<select id="state" class="form-control" name="state">
						<option value="کل شهر">
							کل شهر
						</option>
					</select>	
			    </div>
		  	</div>
		  	<div class="form-group">
			    <label for="addres" class="col-sm-offset-1 col-sm-2 control-label">آدرس پستی</label>
			    <div class="col-sm-8">
			    	<textarea type="text" id="addres" class="form-control" name="address" required></textarea>
			    </div>
		  	</div>
		</div>
		<div class="row">
			<div class="col-sm-8 col-sm-offset-3">
				<button class="btn btn-success btn-block" type="submit"> ذخیره آدرس</button>
			</div>
		</div>
	</form>
	</div>
    <div class="seperate"></div>	
    <div class="seperate"></div>	
	@endif
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-info">
			<div class="panel-heading">
				سفارش شما  در  *** <span class="double-size bold"> {{ \App\Http\Controllers\Controller::NAME}} </span>***
			</div>
	    	<div class="panel-body">
	    		<div class="row">
		    		<div class="col-xs-6">
			    		<label>
			    		شماره فاکتور:
			    		</label>
						{{ \Nopaad\Persian::correct($order->id) }}
					</div>
					<div class="col-xs-6">
						<label>
						تاریخ فاکتور: 
						</label> 
						{{ \Nopaad\jDate::forge( $order->updated_at )->format(' %Y/%m/%d - %H:%M:%S ') }}
					</div>
				</div>	
				<div class="row">
					<div class="col-xs-12">
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<table class="table table-striped table-hover">
						<thead>
						<tr>
							<th>
								تعداد	
							</th>
							<th>
								نام غذا
							</th>
							<th>
								قیمت
							</th>
							<th>
								با تخفیف
							</th>
							<th>
								مبلغ
							</th>
						</tr>
						</thead>
						<tbody>
						@foreach($products->unique('name') as $product)
						<tr>
							<td>
								{{  \Nopaad\Persian::correct($products->where('name',$product->name)->count()) }}
								<a class="btn btn-xs btn-info" href="/type/{{$product->type_id}}">
								<span class="glyphicon glyphicon-edit"></span>
								</a>
							</td>
							<td>
								{{ $product->name }}
							</td>
							<td>
								{{ \Nopaad\Persian::correct( number_format($product->price, 0, '',',') ) }}
							</td>
							<td>
								{{  \Nopaad\Persian::correct( number_format( $product->price * (100 - \App\Http\Controllers\Controller::OFF ) / 100) , 0, '',',')  }}
							</td>
							<td>
								{{  \Nopaad\Persian::correct( number_format(
								$products->where('name',$product->name)->count() * $product->price * (100 - \App\Http\Controllers\Controller::OFF ) / 100
								, 0, '',',')) }}
							</td>
						</tr>
						@endforeach
						</tbody>
						</table>
					</div>
					<div class="col-sm-6">
						<div class="seperate"></div>
						<dl class="dl-horizontal">
						  	<dt>جمع کل هزینه ها</dt>
						  	<dd>{{  \Nopaad\Persian::correct(  number_format( $products->sum('price'), 0, '',',') ) }} تومان</dd>
						  	<div class="half-seperate"></div>
						  	<dt>جمع هزینه ها پس از تخفیف</dt>
						  	<dd>{{  \Nopaad\Persian::correct(  number_format($products->sum('price') * (100 - \App\Http\Controllers\Controller::OFF ) / 100 , 0, '',',')) }} تومان</dd>
						  	<div class="half-seperate"></div>
						  	<dt>هزینه ارسال</dt>
						  	<dd>{{ \Nopaad\Persian::correct( number_format( \App\Http\Controllers\Controller::PEYK ), 0, '',',') }} تومان</dd>
						  	<div class="half-seperate"></div>
						  	<dt class="double-size">هزینه قابل پرداخت</dt>
						  	<dd class="double-size">{{ \Nopaad\Persian::correct(  number_format( $products->sum('price') * (100 - \App\Http\Controllers\Controller::OFF ) / 100 + \App\Http\Controllers\Controller::PEYK , 0, '',',')) }} تومان</dd>
						  	<div class="seperate"></div>
						</dl>
					</div>
				</div>
	    	</div>
	    </div>
	</div>
</div>
<div class="modal fade" id="sms-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">فراموشی رمز عبور</h4>
            </div>
            <div class="modal-body">
                <form action="/forget-password" method="post">
                    {{ csrf_field() }}
                    <p class="bold">
                        شماره همراه:
                    </p>
                    <input type="text" name="phone" class="form-control">
                    <div class="help-block">مثال: 09123456789</div>
                    <div class="half-seperate"></div>
                    <button class="btn btn-success btn-block">ارسال رمز عبور جدید</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="seperate"></div>
@endsection
@push('script')
<!-- <script type="text/javascript">
    select = document.getElementById('city');
	$("#province").change(function () {
		$.get('/api/city/'+this.value, function( data ) {
		  	for (var i=0; i<15; i++){
	     		select.remove(0);
		  	}
			data.forEach(function(item){
			    var opt = document.createElement('option');
			    opt.value = item.id;
			    opt.innerHTML = item.name;
			    select.appendChild(opt);				
			});
		});
  	});
</script> -->

<script type="text/javascript">
    select = document.getElementById('city');
	$("#province").change(function () {
		getCity(this.value);
  	});
	function getCity(ostan)
	{
		$.get('/api/city/'+ostan, function( data ) {
		  	for (var i=0; i<15; i++){
	     		select.remove(0);
		  	}
			data.forEach(function(item){
			    var opt = document.createElement('option');
			    opt.value = item.id;
			    opt.innerHTML = item.name;
			    select.appendChild(opt);				
			});
		});
	}
	getCity(1);
</script>

@endpush