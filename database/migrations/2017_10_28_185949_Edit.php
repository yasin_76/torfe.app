<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Edit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
public function up()
{
    Schema::table('startups', function (Blueprint $table) {
        // $table->string('full_name')->after('last_name')->unique();
        $table->text('yasin-bazar')->nullable();
        $table->text('yasin-problem')->nullable();
        $table->text('yasin-model')->nullable();
        $table->text('yasin-raghib')->nullable();
        $table->text('yasin-upload')->nullable();
    });
}

/**
 * Reverse the migrations.
 *
 * @return void
 */
public function down()
{
    Schema::table('startup', function (Blueprint $table) {
        $table->dropColumn('yasin-bazar');
        $table->dropColumn('yasin-problem');
        $table->dropColumn('yasin-model');
        $table->dropColumn('yasin-raghib');
        $table->dropColumn('yasin-bazar');
        $table->dropColumn('yasin-bazar');
        $table->dropColumn('full_name');
        $table->dropColumn('full_name');
        $table->dropColumn('full_name');
        $table->dropColumn('full_name');
        $table->dropColumn('full_name');
    });
}
}