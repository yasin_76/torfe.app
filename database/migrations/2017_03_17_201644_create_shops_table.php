<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('name_en');
            $table->string('phone');
            $table->string('email');
            $table->integer('address_id')->unsigned()->nullable();
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->integer('image_id')->unsigned()->nullable(); // logo
            $table->foreign('image_id')->references('id')->on('images');
            $table->string('merchant_code_zarinpal')->nullable();
            $table->integer('page_size')->unsigned()->default(15);
            $table->string('meta_title');
            $table->string('meta_keyword');
            $table->string('meta_description');            
            $table->string('sms_user')->nullable();
            $table->string('sms_phone')->nullable();
            $table->string('sms_pass')->nullable();
            $table->string('email_user')->nullable();
            $table->string('email_pass')->nullable();
            $table->string('telegram')->nullable();
            $table->string('instagram')->nullable();
            $table->text('about')->nullable();
            $table->string('start_time')->nullable();
            $table->string('end_time')->nullable();
            $table->string('credit_card')->nullable();
            $table->integer('payk')->nullable();
            $table->integer('off')->unsigned()->default(0);
            $table->integer('minimum_price')->unsigned()->default(5000);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            // $table->boolean('emkanekharid')->default(1);
            // $table->string('url')->nullable();
            // $table->string('web_log')->nullable();
            // $table->string('fav-icon')->nullable();
            // $table->string('mahdode_ersale_kala')->nullable();
            // $table->integer('sabeghe')->unsigned()->nullable();
            // $table->integer('salary')->unsigned()->nullable(); // dar amad mahane
            // $table->integer('mall_type_id')->unsigned()->nullable();
            // $table->foreign('mall_type_id')->references('id')->on('malls_types');
            // $table->integer('theme_id')->unsigned()->nullable();
            // $table->foreign('theme_id')->references('id')->on('themes');
            $table->timestamps();
            $table->softDeletes();

            // data base slider images ham besaz
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
