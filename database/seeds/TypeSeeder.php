<?php

use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types =[
            ['name' => 'رستوران'],
            ['name' => 'کافی شاپ'],
            ['name' => 'بسته بندی'],
            ['name' => 'بهداشتی'],
            ['name' => 'خدمات'],
            ['name' => 'سایرین'],
            ['name' => 'لیوان', 'type_id' => 1],
            ['name' => 'بشقاب', 'type_id' => 1],
            ['name' => 'قاشق', 'type_id' => 1],
            ['name' => 'دیس', 'type_id' => 1],
            ['name' => 'کاسه', 'type_id' => 1],
            ['name' => 'سایر ظروف', 'type_id' => 1],
            ['name' => 'لیوان', 'type_id' => 2],
            ['name' => 'بشقاب', 'type_id' => 2],
            ['name' => 'قاشق', 'type_id' => 2],
            ['name' => 'دیس', 'type_id' => 2],
            ['name' => 'کاسه', 'type_id' => 2],
            ['name' => 'سایر ظروف', 'type_id' => 2],

            ['name' => 'لیوان', 'type_id' => 3],
            ['name' => 'بشقاب', 'type_id' => 3],
            ['name' => 'قاشق', 'type_id' => 3],
            ['name' => 'دیس', 'type_id' => 3],
            ['name' => 'کاسه', 'type_id' => 3],
            ['name' => 'سایر ظروف', 'type_id' => 3],

            ['name' => 'لیوان', 'type_id' => 4],
            ['name' => 'بشقاب', 'type_id' => 4],
            ['name' => 'قاشق', 'type_id' => 4],
            ['name' => 'دیس', 'type_id' => 4],
            ['name' => 'کاسه', 'type_id' => 4],
            ['name' => 'سایر ظروف', 'type_id' => 4],

            ['name' => 'لیوان', 'type_id' => 5],
            ['name' => 'بشقاب', 'type_id' => 5],
            ['name' => 'قاشق', 'type_id' => 5],
            ['name' => 'دیس', 'type_id' => 5],
            ['name' => 'کاسه', 'type_id' => 5],
            ['name' => 'سایر ظروف', 'type_id' => 5],

            ['name' => 'لیوان', 'type_id' => 6],
            ['name' => 'بشقاب', 'type_id' => 6],
            ['name' => 'قاشق', 'type_id' => 6],
            ['name' => 'دیس', 'type_id' => 6],
            ['name' => 'کاسه', 'type_id' => 6],
            ['name' => 'سایر ظروف', 'type_id' => 6],
        ];
        $i = 1;
        foreach($types as $type)
        {
           App\Models\Type::updateOrCreate(['id' => $i ] ,$type);
           $i ++;
        }
        // foreach($types as $type)
        // {
    	   // App\Models\Type::firstOrCreate($type); 
        // }
    }
}
