<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','deleted_at','updated_at'];  
}
